package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.model.Admin;
import com.model.vendor;

@Component("AdminDaoImpl")
public class AdminDaoImpl implements AdminDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	public AdminDaoImpl(DataSource dataSoruce) {
		jdbcTemplate = new JdbcTemplate(dataSoruce);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	
	@Override
	public void createAdmin(Admin admin) {

		String sql = "insert into admin(fname,lname,email,phone,pass) values(?,?,?,?,?)";

		try {

		 jdbcTemplate.update(sql,new Object[] { admin.getFirstName(), admin.getLastName(), admin.getEmailId(),admin.getContactNumber(),admin.getPassword() });


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Admin> read() {
		List<Admin> UserList = jdbcTemplate.query("SELECT * FROM admin", new RowMapper<Admin>() {

			@Override
			public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
				Admin admin = new Admin();

				admin.setEmailId(rs.getString("email"));
				admin.setPassword(rs.getString("pass"));

				return admin;
			}

		});

		return UserList;
	}

	@Override
	public List<vendor> read1() {
		List<vendor> UserList = jdbcTemplate.query("SELECT * FROM vendor", new RowMapper<vendor>() {

			@Override
			public vendor mapRow(ResultSet rs, int rowNum) throws SQLException {
				vendor admin = new vendor();

				admin.setVendorid(rs.getString("id"));

				return admin;
			}

		});

		return UserList;
	}
	@Override
	public int deleteAdmin(String email) {
		String sql = "delete from vendor where id =?";
		int flag = jdbcTemplate.update(sql, email);
		return flag;
	}
	

	
}
