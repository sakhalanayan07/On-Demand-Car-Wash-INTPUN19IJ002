package com.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.dao.VenderDao;
import com.dao.VendorDaoImpl;
import com.dao.WashingDao;
import com.dao.WashingDaoImpl;
import com.dao.BookingDao;
import com.dao.BookingDaoImpl;

@Configuration
public class DBConfig {

	@Bean
	DriverManagerDataSource getDataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/carwash?autoReconnect=true&useSSL=false");
		ds.setUsername("root");
		ds.setPassword("admin");

		return ds;
	}
     
	@Bean
	 public JdbcTemplate jdbcTempolate()
	 {
		 JdbcTemplate jdbcTemplate = new JdbcTemplate();
		 jdbcTemplate.setDataSource(getDataSource());
         return jdbcTemplate;
	 }
	 
	 @Bean
	 @Primary
		public UserDao StudentDao() {
		 UserDaoImpl student = new UserDaoImpl(getDataSource());
			return student;
		}
	 
	 @Bean
		public VenderDao VendorDao() {
		 VendorDaoImpl student1 = new VendorDaoImpl(getDataSource());
			return student1;
		}
	 
	 @Bean
		public WashingDao WashingDao() {
		 WashingDaoImpl student2 = new WashingDaoImpl(getDataSource());
			return student2;
		}
	 
	 @Bean
		public BookingDao BookingDao() {
		 BookingDaoImpl student3 = new BookingDaoImpl(getDataSource());
			return student3;
		}
}
