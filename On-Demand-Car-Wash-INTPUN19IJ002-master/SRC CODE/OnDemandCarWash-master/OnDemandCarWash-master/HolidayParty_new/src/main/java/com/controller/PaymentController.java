package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller
public class PaymentController {
	@RequestMapping(value="/payment",method=RequestMethod.GET)
	public String performRegistration(	) {
		return "payment";
	}
	@RequestMapping(value="/creditcard",method=RequestMethod.GET)
	public String creditCardPage(	) {
		return "creditcard";
	}
	@RequestMapping(value="/debitcard",method=RequestMethod.GET)
	public String debitCardPage(	) {
		return "debitcard";
	}
	@RequestMapping(value="/upi",method=RequestMethod.GET)
	public String UPIPage(	) {
		return "upi";
	}

	
}
