package com.dao;

import java.util.List;

import com.model.Admin;
import com.model.vendor;

public interface AdminDao {

	public void createAdmin(Admin admin);
    
	public List<Admin> read();
	public List<vendor> read1();
	public int deleteAdmin(String email);
	
}
