package com.dao;

import java.util.List;

import com.model.Booking;
import com.model.User;
import com.model.Washing;

public interface BookingDao {
     public void addbooking(Booking b1,int a,int b);
     public int getID(String id);
     public List<Booking> read(int id);
     public void delete(String addr);
     public List<Washing> read();
     public int getID2(Booking u1,String id);
     public void addpayment(Booking b1,int a,int b);
     public void updatepayment(int b);
     public int getamount(int b);
     public int getID3(String ADDR);
     public void deletePay(int pqr);
}
