package com.model;

import java.sql.Date;
import java.sql.Time;

import org.springframework.stereotype.Component;

@Component
public class Booking {
   private String washingserv;
   private String timeslot;
   private String date;
   private String sel;
   private String address;
public String getWashingserv() {
	return washingserv;
}
public void setWashingserv(String washingserv) {
	this.washingserv = washingserv;
}
public String getTimeslot() {
	return timeslot;
}
public void setTimeslot(String timeslot) {
	this.timeslot = timeslot;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getSel() {
	return sel;
}
public void setSel(String sel) {
	this.sel = sel;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
   

   
}
