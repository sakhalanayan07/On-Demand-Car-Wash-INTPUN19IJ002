package com.dao;

import java.util.Date;
import java.util.List;

import com.model.Booking;
import com.model.User;
import com.model.Washing;
import com.model.vendor;

public interface VenderDao {
	
	public void createvendor(vendor vendor1);
	public List<vendor> read();
	public void update(vendor v1,String id);
	 public int getID1(Washing w1,String id);
	 public int getID2(Washing w1,int id);
	 public List<Booking> read2(int abc);
	 public void delete(String  addr);
	  public int getID3(String ADDR);
	     public void deletePay(int pqr);
}
