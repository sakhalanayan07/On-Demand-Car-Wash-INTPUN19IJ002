package com.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dao.BookingDao;
import com.dao.UserDao;
import com.model.Booking;
import com.model.User;
	
@Controller
	public class UserController {

		@Autowired
		private UserDao userDao;
		static String email;
		
		@Autowired
		private BookingDao bookDao;
	
		@RequestMapping(value="/index",method=RequestMethod.GET)
		public String showPage(@ModelAttribute("user") User user){
		user=new User();
			return "index";
		}
		
		@RequestMapping(value="/userlogin",method=RequestMethod.GET)
		public String performlogin(@ModelAttribute("user") User user,
				BindingResult result) {
			return "userlogin";

		}
	
		
		@RequestMapping(value="/userregistration",method=RequestMethod.GET)
		public String performRegistration(@ModelAttribute("user") User user,
				BindingResult result) {
			if(result.hasErrors()){
				
			}
			return "userregistration";

		}
		@RequestMapping(value="/logout",method=RequestMethod.GET)
		public String logoutPage(@ModelAttribute("user") User user,
				BindingResult result) {
			
			return "logout";

		}
		
		
		@RequestMapping(value="/register",method=RequestMethod.POST)
		public String registerPage( @ModelAttribute("user") @Valid User user,BindingResult result,ModelMap model) {
		   
			if (result.hasErrors()) {
				return "userregistration";
			}
			
			userDao.create(user);
			model.addAttribute("msg", "User registration successful.");

			return "userregistration";
		}
		
		@RequestMapping(value="/first",method=RequestMethod.POST)
		public String loginPage( @ModelAttribute("user") User user,BindingResult result,ModelMap model) {
		   
			if (result.hasErrors()) {
				return "userlogin";
			}
			
			List<User> listStudent = userDao.read();
		    System.out.println(listStudent);
		    System.out.println("dfgdfgdfg");
			for (User user2 : listStudent) {
				if(user2.getEmailId().equals(user.getEmailId())  &&  user2.getPassword().equals(user.getPassword()))
				{
					email = user.getEmailId();
					int id = userDao.getID(email);
					try{
					String qwe = userDao.getStatus(id);
					
					if(qwe.equals("Pending"))
					{
						model.addAttribute("msg1", "Payment is Pending");
						return "first";	
					}
					return "first";
					}catch(Exception e)
					{
						return "first";
					}
				}
			}
			
			return "userlogin";

		}
		
		@RequestMapping(value = "/bookinghistory")
		public ModelAndView readBooking(@ModelAttribute("user") User user,ModelAndView model) throws IOException {
			 
			System.out.println("Hello" +email);
            int abc = bookDao.getID( email);
            System.out.println("Hello" + abc);
            List<Booking> listStudent = bookDao.read(abc);
			model.addObject("listStudent", listStudent);
			model.setViewName("bookinghistory");

			return model;
		}
		
		@RequestMapping(value="/first1",method=RequestMethod.GET)
		public String loginPage1( @ModelAttribute("user") User user,BindingResult result,ModelMap model) {
		   
			if (result.hasErrors()) {
				return "userlogin";
			}
		
			return "first";

		}
		
	}


