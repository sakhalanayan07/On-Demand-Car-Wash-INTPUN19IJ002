package com.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dao.BookingDao;
import com.dao.VenderDao;
import com.dao.WashingDao;
import com.model.Booking;
import com.model.User;
import com.model.Washing;
import com.model.vendor;
	
@Controller
	public class VendorController {
	
	@Autowired
	private VenderDao venderDao;
	
	@Autowired
	private WashingDao WashDao;
	
	
	static String id;
	static int abc;
	
	
	@RequestMapping(value="/vendorregistration",method=RequestMethod.GET)
	public String performRegistration(@ModelAttribute("vendor1") vendor vendor1,
			BindingResult result) {
		return "/vendorregistration";

	}
	@RequestMapping(value="/vendorlogin",method=RequestMethod.GET)
	public String performlogin(@ModelAttribute("vendor1") vendor vendor1,
			BindingResult result) {
		return "vendorlogin";

	}
	@RequestMapping(value="/updatevendor",method=RequestMethod.GET)
	public String performupdate(@ModelAttribute("vendor1") vendor vendor1,
			BindingResult result) {
		return "vendorupdate";

	}
	
	@RequestMapping(value="/vendorregister",method=RequestMethod.POST)
	public String registerPage( @ModelAttribute("vendor1") @Valid vendor vendor1,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			return "vendorregistration";
		}
		//id=vendor1.getVendorid();
		venderDao.createvendor(vendor1);
		model.addAttribute("msg", "Vendor registration successful.");

		return "vendorregistration";
	}
	
	@RequestMapping(value="/second",method=RequestMethod.POST)
	public String loginPage( @ModelAttribute("vendor1") vendor vendor1,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			return "vendorlogin";
		}
		
		List<vendor> listStudent = venderDao.read();
		for (vendor vendor : listStudent) {
			
			if(vendor.getVendorid().equals(vendor1.getVendorid())  &&  vendor.getPassword().equals(vendor1.getPassword()))
			{
				id=vendor1.getVendorid();
				return "second";
			}
		}
		return "vendorlogin";

	}
	
	@RequestMapping(value="/second1",method=RequestMethod.GET)
	public String loginPage1( @ModelAttribute("vendor1") vendor vendor1,BindingResult result,ModelMap model) {
	   
//		if (result.hasErrors()) {
//			return "vendorlogin";
//		}
//		
//		List<vendor> listStudent = venderDao.read();
//		for (vendor vendor : listStudent) {
//			
//			if(vendor.getVendorid().equals(vendor1.getVendorid())  &&  vendor.getPassword().equals(vendor1.getPassword()))
//			{
//				id=vendor1.getVendorid();
//				return "second";
//			}
//		}
		return "vendorlogin";

	}
	
	@RequestMapping(value="/vendorupdation",method=RequestMethod.POST)
	public String UpdatePage( @ModelAttribute("vendor1") vendor vendor1,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			System.out.println("Hello" +id);
			return "second";
		}
		System.out.println("Hello11111111111" +id);
		venderDao.update(vendor1,id);
		model.addAttribute("msg", "Updation successful.");

		return "second";
	}
	
	@ModelAttribute("genderList")
	public List<String> populateExpense() {
		List<String> gender = new ArrayList<String>();
		gender.add("Male");
		gender.add("Female");
		gender.add("Other");
		return gender;
	}
	@RequestMapping(value="/addwashing",method=RequestMethod.POST)
	public String registerPage(@ModelAttribute("washing") Washing w1 ,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			System.out.println("qwerty");
			System.out.println("dsfsd" + w1.getClosetime());
			return "second";
		}
		 abc =  WashDao.getID(w1,id);
		 System.out.println("dsfsd" + w1.getClosetime());
		WashDao.createwashing(w1,abc);
		model.addAttribute("msg", "Student registration successful.");

		return "second";
	}
	
	@RequestMapping(value="/washingupdate",method=RequestMethod.POST)
	public String UpdatePage( @ModelAttribute("washing") Washing w1,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			System.out.println("hello");
			return "second";
			
		}
		 abc =  WashDao.getID(w1,id);

		System.out.println("hello11" +abc);
		WashDao.updatewashing(w1,abc);
		model.addAttribute("msg", "Student registration successful.");

		return "second";
	}
	
	@RequestMapping(value = "/vendorbooking")
	public ModelAndView readBooking(@ModelAttribute("washing") Washing w1,ModelAndView model) throws IOException {
		 
		System.out.println("Hello" +id);
        int abc = venderDao.getID1(w1, id);
        System.out.println("Hello" + abc);
        
        int xyz = venderDao.getID2(w1, abc);
        System.out.println("Hello" + xyz);
        
        List<Booking> listStudent = venderDao.read2(xyz);
		model.addObject("listStudent", listStudent);
		model.setViewName("vendorbooking");

		return model;
	}
	
	@RequestMapping(value = "/rejection/{address}")
	public ModelAndView deleteStudentById(ModelAndView mv, @PathVariable("address") String addr)
			throws IOException {
        System.out.println("qwerty");
        int pqr = venderDao.getID3( addr);
        venderDao.delete(addr);
        venderDao.deletePay(pqr);
		mv.setViewName("rejection");

		return mv;
	}
	}


