package com.dao;


import java.util.List;

import com.model.User;
import com.model.Washing;

public interface UserDao {

	public void create(User user);
	
	public List<User> read();
	public String getStatus(int id);
	public int getID(String id);

	
}