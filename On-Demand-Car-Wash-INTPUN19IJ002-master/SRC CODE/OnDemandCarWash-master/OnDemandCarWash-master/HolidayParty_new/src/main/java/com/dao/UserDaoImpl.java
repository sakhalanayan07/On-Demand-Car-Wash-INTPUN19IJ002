package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import com.model.User;
import com.model.Washing;


@Component("UserDaoImpl")
public class UserDaoImpl implements UserDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	
	public UserDaoImpl(DataSource dataSoruce) {
		jdbcTemplate = new JdbcTemplate(dataSoruce);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void create(User user) {

		String sql = "insert into user(fname,lname,email,phone,pass) values(?,?,?,?,?)";

		try {

		 jdbcTemplate.update(sql,new Object[] { user.getFirstName(), user.getLastName(), user.getEmailId(),user.getContactNumber(),user.getPassword() });


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<User> read() {
		List<User> UserList = jdbcTemplate.query("SELECT * FROM user", new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User User = new User();

				
				
				User.setEmailId(rs.getString("email"));
				User.setPassword(rs.getString("pass"));

				return User;
			}

		});

		return UserList;
	}
	@Override
	public String getStatus(int id)
	{
		String sql = "SELECT paystatus from payment where userId=?";

		try {

		 String abc = jdbcTemplate.queryForObject(sql,new Object[] { id},String.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public int getID(String id){

		String sql = "SELECT userId from user where email=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] { id},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	

	

	

}