package com.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dao.BookingDao;
import com.model.Admin;
import com.model.Booking;
import com.model.Washing;
import com.model.vendor;

@Controller
public class BookingController {

	@Autowired
	private BookingDao bookDao;
	
	static int abc;
	static int xyz;
	UserController U1 = new UserController();
	
	@RequestMapping(value="/slotbook",method=RequestMethod.GET)
	public String performRegistration(@ModelAttribute("book1") Booking book,
			BindingResult result) {
		return "/slotbooking";

	}
	
	@RequestMapping(value="/book",method=RequestMethod.POST)
	public String registerPage( @ModelAttribute("book1") Booking book,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			System.out.println("sdafgjdgfh5865");
			return "first";
		}
		System.out.println("sdafgjdgf777777");
		 abc = bookDao.getID(U1.email);
		 xyz = bookDao.getID2(book, book.getWashingserv());
		 
		 System.out.println("hello1"+abc);
		 System.out.println("hello2" + xyz + " " +book.getWashingserv());
		 
		bookDao.addbooking(book ,abc,xyz);
		System.out.println("NEWWWW");
		bookDao.addpayment(book ,abc,xyz);
		model.addAttribute("msg", "Booking successful.");

		return "slotbooking";
	}
	
	@RequestMapping(value = "/cancel/{address}")
	public ModelAndView deleteStudentById(ModelAndView mv, @PathVariable("address") String addr)
			throws IOException {
        System.out.println("qwerty");
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAA" + addr);
		int pqr = bookDao.getID3( addr);
		bookDao.delete(addr);
		bookDao.deletePay(pqr);
		mv.setViewName("cancel");

		return mv;
	}
	
	@ModelAttribute("productTypeList")
	public List<String> populateProductType() {

		List<Washing> p1 = bookDao.read();
		List<String> productTypeList = new ArrayList<String>();
		for (Washing washing : p1) {
			productTypeList.add(washing.getLocation());
		}
		return productTypeList;
	}
	
	@RequestMapping(value="/paysuccess",method=RequestMethod.POST)
	public String paymentSuccessfullPage(@ModelAttribute("book1") Booking book,BindingResult result,ModelMap model) {
		 abc = bookDao.getID(U1.email);		 
		bookDao.updatepayment(abc);
		int xyz = bookDao.getamount(abc);
		
		model.addAttribute("msg",xyz);
		return "paysuccess";
	}
	
	
	
}
