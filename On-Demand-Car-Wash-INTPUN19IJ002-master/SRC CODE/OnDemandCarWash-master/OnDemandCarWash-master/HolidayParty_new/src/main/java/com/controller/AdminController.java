package com.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dao.AdminDao;
import com.model.Admin;
import com.model.AdminVendorId;
import com.model.Washing;
import com.model.vendor;

@Controller
public class AdminController {

	@Autowired
	private AdminDao adminDao;
	
	@RequestMapping(value="/adminregistration",method=RequestMethod.GET)
	public String performRegistration(@ModelAttribute("admin") Admin admin,
			BindingResult result) {
		return "/adminregistration";

	}
	
	@RequestMapping(value="/adminregister",method=RequestMethod.POST)
	public String registerPage( @ModelAttribute("admin") @Valid Admin admin,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			return "adminregistration";
		}
		
		adminDao.createAdmin(admin);
		model.addAttribute("msg", "Admin registration successful.");

		return "adminregistration";
	}
	
	@RequestMapping(value="/adminlogin",method=RequestMethod.GET)
	public String performlogin(@ModelAttribute("admin") Admin admin,
			BindingResult result) {
		return "adminlogin";

	}
	
	@RequestMapping(value="/third",method=RequestMethod.POST)
	public String loginPage( @ModelAttribute("admin") Admin admin,BindingResult result,ModelMap model) {
	   
		if (result.hasErrors()) {
			return "adminlogin";
		}
		
		List<Admin> listStudent = adminDao.read();
	    System.out.println(listStudent);
	    System.out.println("dfgdfgdfg");
		for (Admin admin2 : listStudent) {
			
			if(admin2.getEmailId().equals(admin.getEmailId())  &&  admin2.getPassword().equals(admin.getPassword()))
			{
				return "third";
			}
		}
		return "adminlogin";

	}

	@RequestMapping(value = "/vendardelete", method= RequestMethod.GET)
	public String deleteVendor(@ModelAttribute("adminVendorId") AdminVendorId AVId, BindingResult result, ModelMap model){
		//System.out.println(AVId.getEmail());
		return "vendardelete";
	}
	@RequestMapping(value  = "/message", method = RequestMethod.POST)
	public String deleteMessage(@ModelAttribute("adminVendorId") AdminVendorId AVId, BindingResult result, ModelMap model){
		System.out.println(AVId.getEmail());
		String email = AVId.getEmail();
		int n = adminDao.deleteAdmin(email);
		System.out.println(n);
		return "message";
	}
    
	@ModelAttribute("vendorTypeList")
	public List<String> populateProductType(@ModelAttribute("vendor1") vendor vendor1) {

		List<vendor> p1 = adminDao.read1();
		List<String> productTypeList = new ArrayList<String>();
		for (vendor admin : p1) {
			productTypeList.add(admin.getVendorid());
		}
		return productTypeList;
	}
	
}
