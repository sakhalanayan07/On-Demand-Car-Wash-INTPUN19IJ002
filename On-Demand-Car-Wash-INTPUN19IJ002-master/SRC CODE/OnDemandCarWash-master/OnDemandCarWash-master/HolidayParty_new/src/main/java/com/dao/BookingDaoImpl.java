package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.model.Booking;
import com.model.User;
import com.model.Washing;
import com.model.vendor;

public class BookingDaoImpl implements BookingDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public BookingDaoImpl(DataSource dataSoruce) {
		jdbcTemplate = new JdbcTemplate(dataSoruce);
	}

	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
    
	@Override
	public void addbooking(Booking book,int a,int b) {
		String sql = "insert into bookings(userId,idwashingCenter,timeSlot,address,date) values(?,?,?,?,?)";

		try {

			 jdbcTemplate.update(sql,
					new Object[] {a,b,book.getTimeslot(),book.getAddress(),book.getDate()});


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Booking> read(int id) {
		String sql = "SELECT * FROM bookings where userId=?";
		List<Booking> studentList = jdbcTemplate.query(sql,new Object[] {id}, new RowMapper<Booking>() {
			
			@Override
			public Booking mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				Booking b1 = new Booking();
                b1.setTimeslot(rs.getString("timeSlot"));
                b1.setAddress(rs.getString("address"));
                b1.setDate(rs.getString("date"));
                
				return b1;
			}

		});

		return studentList;
	}
	
	public int getID(String id)
	{
		String sql = "SELECT userId from user where email=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] { id},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	public int getID2(Booking b1,String sel)
	{
		String sql = "SELECT w_id  from washingCenter where location=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] {sel},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	
	
	@Override
	public void delete(String addr) {
		String sql = "delete from bookings where address=?";

		try {

		 jdbcTemplate.update(sql, new Object[] { addr });



		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	 public List<Washing> read()
	{
		String sql = "SELECT * FROM washingCenter";
		List<Washing> studentList = jdbcTemplate.query(sql,new Object[] {}, new RowMapper<Washing>() {
			
			@Override
			public Washing mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				Washing b1 = new Washing();
				
				b1.setLocation(rs.getString("location"));
            
                
				return b1;
			}

		});

		return studentList;
	}
	
	@Override
	public void addpayment(Booking book,int a,int b) {
		String sql = "insert into payment(userId,idwashingCenter,amount,paystatus) values(?,?,?,?)";
         int amount=0;
		try {
             if(book.getSel().equals("P"))
            	 amount = 500;
             else
            	 amount = 300;
			 jdbcTemplate.update(sql,
					new Object[] {a,b,amount,"Pending"});


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void updatepayment(int b) {
		String sql = "update payment set paystatus=? where userId=?";

		try {

			 jdbcTemplate.update(sql,
					new Object[] { "Paid",b });


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int getamount(int b) {
		String sql = "select amount from payment where userId=?";

		try {

			 int abc = jdbcTemplate.queryForObject(sql,new Object[] { b},int.class);
             return abc;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	@Override
	public int getID3(String ADDR)
	{
		String sql = "SELECT idwashingCenter from bookings where address=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] {ADDR},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	
	@Override
	public void deletePay(int pqr) {
		String sql = "delete from payment where idwashingCenter=?";

		try {

			 jdbcTemplate.update(sql, new Object[] { pqr });



		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
