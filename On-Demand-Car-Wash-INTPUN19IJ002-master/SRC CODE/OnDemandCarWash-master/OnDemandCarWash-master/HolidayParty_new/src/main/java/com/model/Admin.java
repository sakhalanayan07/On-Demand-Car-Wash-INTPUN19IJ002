package com.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

import org.springframework.stereotype.Component;

@Component
public class Admin {
	@Pattern(regexp = "^[a-zA-Z]{1,20}$", message = "please check First name must not contain digits and max size is 20 ")
	private String firstName;
	@Pattern(regexp = "^[a-zA-Z]{1,20}$", message = "please check last name must not contain digits and max size is 20 ")
	private String lastName;
	@Pattern(regexp = "^[789]{1}[0-9]{9}$", message = "please give valid mobile number")
	private String contactNumber;
	@Email(message = "please give valid Email Id")
	private String emailId;
	private String password;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
