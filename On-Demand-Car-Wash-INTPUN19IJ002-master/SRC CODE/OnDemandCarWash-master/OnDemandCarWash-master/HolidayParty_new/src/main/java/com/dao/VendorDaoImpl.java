package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.model.Booking;
import com.model.User;
import com.model.Washing;
import com.model.vendor;

public class VendorDaoImpl implements VenderDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public VendorDaoImpl(DataSource dataSoruce) {
		jdbcTemplate = new JdbcTemplate(dataSoruce);
	}

	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public void createvendor(vendor vendor1){

		String sql = "insert into vendor(id,fname,lname,gender,phone,pass,age) values(?,?,?,?,?,?,?)";

		try {

		 jdbcTemplate.update(sql,new Object[] {vendor1.getVendorid() ,vendor1.getFirstName(), vendor1.getLastName(),vendor1.getGender(),vendor1.getContactNumber(),vendor1.getPassword(),vendor1.getAge() });

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<vendor> read() {
		List<vendor> UserList = jdbcTemplate.query("SELECT * FROM vendor", new RowMapper<vendor>() {

			@Override
			public vendor mapRow(ResultSet rs, int rowNum) throws SQLException {
				vendor v1 = new vendor();

				
				
				v1.setVendorid(rs.getString("id"));
				v1.setPassword(rs.getString("pass"));

				return v1;
			}

		});

		return UserList;
	}
	
	@Override
	public void update(vendor v1,String id) {
		String sql = "update vendor set phone=?, id=? where id=?";

		try {

			 jdbcTemplate.update(sql,
					new Object[] { v1.getContactNumber(),v1.getVendorid(),id });


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int getID1(Washing w1,String id)
	{
		String sql = "SELECT vendorId from vendor where id=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] { id},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	@Override
	public int getID2(Washing w1,int id)
	{
		String sql = "SELECT w_id from washingCenter where id=?";

		try {

		 int xyz = jdbcTemplate.queryForObject(sql,new Object[] { id},int.class);
		 return xyz;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	@Override
	public List<Booking> read2(int id) {
		String sql = "SELECT * FROM bookings where idwashingCenter=? ";
		List<Booking> studentList = jdbcTemplate.query(sql,new Object[] {id}, new RowMapper<Booking>() {
			
			@Override
			public Booking mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				Booking b1 = new Booking();
                b1.setTimeslot(rs.getString("timeSlot"));
                b1.setAddress(rs.getString("address"));
                b1.setDate(rs.getString("date"));
                
				return b1;
			}

		});

		return studentList;
	}
	
	@Override
	public void delete(String addr) {
		String sql = "delete from bookings where address=?";

		try {

			int counter = jdbcTemplate.update(sql, new Object[] { addr });



		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int getID3(String ADDR)
	{
		String sql = "SELECT idwashingCenter from bookings where address=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] {ADDR},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
		
	}
	
	@Override
	public void deletePay(int pqr) {
		String sql = "delete from payment where idwashingCenter=?";

		try {

			 jdbcTemplate.update(sql, new Object[] { pqr });



		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
