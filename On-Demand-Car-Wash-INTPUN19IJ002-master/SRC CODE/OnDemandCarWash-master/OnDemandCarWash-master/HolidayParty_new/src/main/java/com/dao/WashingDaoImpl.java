package com.dao;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import com.model.Washing;

public class WashingDaoImpl implements WashingDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public WashingDaoImpl(DataSource dataSoruce) {
		jdbcTemplate = new JdbcTemplate(dataSoruce);
	}

	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	@Override
	public void createwashing(Washing w1,int id){

		String sql = "insert into washingCenter(id,location,phone,opentime,closetime) values(?,?,?,?,?)";

		try {
          System.out.println("dsfsd" + w1.getClosetime());
		 jdbcTemplate.update(sql,new Object[] {id,w1.getLocation(),w1.getPhone(),w1.getOpentime(),w1.getClosetime() });

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void updatewashing(Washing w1,int id) {
		String sql = "update washingCenter set location=?, opentime=?, closetime=?,phone=? where id=?";

		try {

			 jdbcTemplate.update(sql,
					new Object[] { w1.getLocation(),w1.getOpentime(),w1.getClosetime(),w1.getPhone(),id });

			

		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	@Override
	public int getID(Washing w1,String id){

		String sql = "SELECT vendorId from vendor where id=?";

		try {

		 int abc = jdbcTemplate.queryForObject(sql,new Object[] { id},int.class);
		 return abc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	
	
	
	
	

}
