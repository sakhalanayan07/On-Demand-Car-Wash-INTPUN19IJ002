<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>



<body>

<div class = "Header">

<div>
    
 
</div>

  <h1 class="glow">CarWash</h1>

</div>


<div class="Body">
	
		<h1>Login for vendor</h1>

	<form:form action="/second" method="post" modelAttribute="vendor1">
		<table>
			
			<tr>
				<td>Vendor Id:</td>
				<td><form:input path="vendorid" id="vendorid" name="vendorid"/></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><form:input type="password" path="password" id="password" name="password"/></td>
			</tr>
			<tr>
				<td><input class="button" type="submit" value="LOGIN" name="submit" id="submit"/></td>
			</tr>
			
		</table>
	</form:form>
	
</div>

<div class = "Footer">

</div>		

	
</body>
</html>
