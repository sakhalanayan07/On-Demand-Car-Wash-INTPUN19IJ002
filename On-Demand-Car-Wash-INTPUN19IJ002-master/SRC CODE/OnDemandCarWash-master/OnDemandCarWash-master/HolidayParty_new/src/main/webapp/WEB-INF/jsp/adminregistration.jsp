<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>

<body>

<div class = "Header">

<div>
    
 
</div>

  <h1 class="glow">CarWash</h1>

</div>

<div class="Body">
	
		<h1>Registration form for admin</h1>
	
	<form:form action="/adminregister" method="post" modelAttribute="admin">
		<table>
			<tr>
				<td>First Name:</td>
				<td><form:input path="firstName" id="firstName" name="firstName"/></td>
				<td><form:errors path = "firstName" /></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><form:input path="lastName" id="lastName" name="lastName"/></td>
				<td><form:errors path = "lastName" /></td>
			</tr>
			<tr>
				<td>Contact Number:</td>
				<td><form:input path="contactNumber" id="contactNumber" name="contactNumber"/></td>
				<td><form:errors path = "contactNumber" /></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input path="emailId" id="emailId" name="emailId"/></td>
				<td><form:errors path = "emailId" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><form:input type="password" path="password" id="password" name="password"/></td>
				<td><form:errors path = "password" /></td>
			</tr>
			<tr>
				<td><input class="button" type="submit" value="Register" name="submit" id="submit"/></td>
				<c:if test = "${msg != null}">
			<td><a href = "adminlogin" class="button">Login</a></td>
			</c:if>
			</tr>
			<tr>
			<td>${msg}</td> 
			</tr>
		</table>
	</form:form>
	
	</div>
	
<div class = "Footer">

</div>
</body>
</html>
