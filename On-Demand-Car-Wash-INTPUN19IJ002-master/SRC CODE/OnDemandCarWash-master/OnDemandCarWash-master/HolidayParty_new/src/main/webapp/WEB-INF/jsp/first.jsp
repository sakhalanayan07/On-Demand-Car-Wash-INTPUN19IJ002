<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Login</title>
</head>
<body>

<div class = "Header">

<span >
 <h1 class="glow">CarWash</h1>
 </span>
 <span class="logOutBtn">
 <a href="index" class = "button" id = "logout">Logout</a>
  </span>
</div>

<div class ="Body">

<br>
<a href="/slotbook" class ="button">Book A Slot</a>
<br>
<a href="/bookinghistory" class ="button">See your Bookings</a>
<br>
				<c:if test = "${msg1 != null}">
			<a href="/payment" class ="button">Proceed to pay</a>
			</c:if>
			<c:if test = "${msg1 == null}">
			<h2> No Payments Are Pending</h2>
			</c:if>
</div>

<div class = "Footer">

</div>
</body>
</html>