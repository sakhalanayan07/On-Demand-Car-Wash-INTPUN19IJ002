<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Read</title>
</head>
<body>

<div class = "Header">

<div>
    
 
</div>

  <h1 class="glow">CarWash</h1>

</div>

<div class ="Body">
	<h2>Booking History</h2>
	<table border="1">
		<tr>
			<th>Time Slot</th>
			<th>Address</th>
			<th>Date</th>
			<th>Cancel</th>
		</tr>
		<c:forEach var="student" items="${listStudent}">
			<tr>
				<td>${student.timeslot}</td>
				<td>${student.address}</td>
				<td>${student.date}</td>
				<td><a href="cancel/<c:out value='${student.address}'/>">CANCEL</a></td>
			</tr>
		</c:forEach>
		
	</table>
	<a href="/first1" class ="button">Return to Home Page</a>
	</div>
	
<div class = "Footer">

</div>
	
</body>
</html>