<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <style><%@include file="styles.css"%></style>
<title>Vendor Login</title>
</head>
<body>

<div class = "Header">

<div>
    
 
</div>

<span >
 <h1 class="glow">CarWash</h1>
 </span>
 <span class="logOutBtn">
 <a href="index" class = "button" id = "logout">Logout</a>
  </span>
</div>

<div class ="Body">

<a href="/updatevendor" class ="button">Update Vendor</a>
<br>
<a href="/washingservice" class ="button">Add Washing Center</a>
<br>
<a href="/updatewashing" class ="button">Update Washing Center</a>
<br>
<a href="/vendorbooking" class ="button">Show My Bookings</a>

</div>

<div class = "Footer">

</div>
</body>
</html>