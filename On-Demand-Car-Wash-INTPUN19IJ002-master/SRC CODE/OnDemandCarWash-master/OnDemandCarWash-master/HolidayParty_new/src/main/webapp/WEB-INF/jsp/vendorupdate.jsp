<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>On Demand Car Wash</title>
</head>
<body>

<div class = "Header">

<div>
    
 
</div>

  <h1 class="glow">CarWash</h1>

</div>

<div class ="Body">

	<center>
		<h1>Updation form for vendor</h1>
	</center>
	<form:form action="/vendorupdation" method="post" modelAttribute="vendor1">
		<table>
		    <tr>
				<td>Vendor Id:</td>
				<td><form:input path="vendorid" id="vendorid" name="vendorid" required="required"/></td>
				<td><form:errors path = "vendorid" /></td>
			</tr>
			<tr>
				<td>Contact Number:</td>
				<td><form:input path="contactNumber" id="contactNumber" name="contactNumber" required="required"/></td>
				<td><form:errors path = "contactNumber" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Update" name="submit" id="submit" class ="button"/></td>
			</tr>
			<tr>
			<td>${msg}</td> 
			</tr>
		</table>
	</form:form>
	</div>
	
<div class = "Footer">

</div>
	
</body>
</html>
