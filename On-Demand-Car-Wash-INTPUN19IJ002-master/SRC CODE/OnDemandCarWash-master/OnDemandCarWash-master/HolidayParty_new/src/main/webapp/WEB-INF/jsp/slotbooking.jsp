<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>On Demand Car Wash</title>
</head>
<body>
<div class = "Header">

<div>
    
 
</div>

  <h1 class="glow">CarWash</h1>

</div>

<div class ="Body">

	
		<h1>Slot Booking</h1>
	
	<form:form action="/book" method="post" modelAttribute="book1">
		<table>
		<tr>
				<td><label>Washing Center</label></td>
				<td><form:select path="washingserv">
						<form:options items="${productTypeList}" />
	 				</form:select>
	 			</td>
			</tr>
		<tr>
               <td><form:label path = "timeslot">PickUp/Drop</form:label></td>
               <td>
                  <form:radiobutton path = "timeslot" value = "1" label = "10AM : 12PM" />
                  <form:radiobutton path = "timeslot" value = "2" label = "1PM : 3PM" />
                    <form:radiobutton path = "timeslot" value = "3" label = "4PM : 6PM" />
               
               </td>
            </tr> 	
			
			<tr>
				<td>Date(Format =(yyyy-mm-dd))</td>
				<td><form:input path="date" id="date" name="date" required="required"/></td>
				<td><form:errors path = "date" /></td>
			</tr>
			<tr>
               <td><form:label path = "sel">PickUp/Drop</form:label></td>
               <td>
                  <form:radiobutton path = "sel" value = "P" label = "PickUp" />
                  <form:radiobutton path = "sel" value = "D" label = "Drop" />
               </td>
            </tr> 	
			<tr>
				<td>Address:</td>
				<td><form:input path="address" id="address" name="address" required="required"/></td>
				<td><form:errors path = "address" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Book Slot" class ="button" name="submit" id="submit"/></td>
			</tr>
			<tr>
			<td>${msg}</td> 
			</tr>
			<tr>
			<td><a href="/index" class ="button">Return to Home Page</a></td> 
			</tr>
		</table>
	</form:form>
	
	</div>
	
<div class = "Footer">

</div>	
</body>
</html>
