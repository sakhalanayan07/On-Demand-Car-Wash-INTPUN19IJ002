<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style><%@include file="styles.css"%></style>
<title>Insert title here</title>
</head>
<body>

<div class = "Header">

<div>
    
 
</div>


 <span >
 <h1 class="glow">CarWash</h1>
 </span>
 <span class="logOutBtn">
 <a href="index" class = "button" id = "logout">Logout</a>
  </span>
</div>

<div class ="Body">
<form:form modelAttribute="vendor1">
<table>
		<tr>
				<td><label>Vendor List</label></td>
				<td><form:select path="vendorid">
						<form:options items="${vendorTypeList}" />
	 				</form:select>
	 			</td>
			</tr>
		<tr>
		</tr>
		</table>
		</form:form>
		<br><br><br>
<a href="vendardelete" class ="button">Delete Vendor</a>

</div>

<div class = "Footer">

</div> 
</body>
</html>
