<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <style><%@include file="styles.css"%></style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>On Demand Car Wash</title>
</head>
<body>

<div class = "Header">

<div>
    
 
</div>

  <h1 class="glow">CarWash</h1>

</div>

<div class ="Body">
	<center>
		<h1>Updation form for Washing Center</h1>
	</center>
	<form:form action="/washingupdate" method="post" modelAttribute="washing">
		<table>
		<tr>
				<td>Location</td>
				<td><form:input path="location" id="location" name="location" required="required"/></td>
				<td><form:errors path = "location" /></td>
			</tr>
			
			<tr>
				<td>Open Time</td>
				<td><form:input path="opentime" id="opentime" name="opentime" required="required"/></td>
				<td><form:errors path = "opentime" /></td>
			</tr>
			<tr>
				<td>Close Time</td>
				<td><form:input path="closetime" id="closetime" name="closetime" required="required"/></td>
				<td><form:errors path = "closetime" /></td>
			</tr>
			<tr>
				<td>Contact Number:</td>
				<td><form:input path="phone" id="phone" name="phone" required="required"/></td>
				<td><form:errors path = "phone" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Update Washing Service" name="submit" id="submit" class ="button"/></td>
			</tr>
			<tr>
			<td>${msg}</td> 
			</tr>
		</table>
	</form:form>
	</div>
	
<div class = "Footer">

</div>
</body>
</html>
